# TreeFTP

Affichage de l'arborescence d'un répertoire distant sur la sortie standard à l'aide du protocole applicatif FTP.
Arthur ASSiMA
01/2021

# Introduction

Ce projet permet d'afficher de manière similaire à la commande tree l'arborescence d'un répertoire distant via FTP.
On y utilisera notamment des Sockets TCP en Java pour afficher récursivement l'arborescence du répertoire distant en profondeur.
 
# Architecture

Le projet contient deux classes principales TreeFtp et Canal.

Canal permet de se connecter et de communiquer avec un serveur ftp à l'aide de sockets, inputstreams, outpustreams, bufferedreader et printwriter. 

La classe TreeFtp permet par le biais de Canal à envoyer les commandes nécessaires dans le but d'afficher l'arborescence du répertoire distant.

On retrouve un polymorphisme dans l'instanciation de TreeFtp et de Canal car l'utilisateur peut choisir de s'identifier anonymement ou avec ses propres identifiants.

Une instance de TreeFtp crée un Canal à sa construction pour communiquer avec le serveur ftp et en créera un deuxième pour récupérer les données qu'il enverra quand le mode passif sera activé.
TreeFtp contient deux méthodes principales, une pour se connecter au serveur et une autre pour afficher le répertoire distant.

Plusieurs erreurs peuvent être détectées lors de la création d'un Canal telles que la perte de connexion ou encore des identifiants incorrects.
L'affichage de l'arborescence peut également détécter des fichiers corrompus mais on ignorera ces fichiers dans un catch pour poursuivre l'arborescence.

On dispose enfin d'une classe Main qui analyse les arguments entrés par l'utilisateur pour se connecter au serveur avec ou sans identifiants, choisir une profondeur maximale (-L dans la commande tree) ou encore afficher les informations du fichier (-p dans la commande tree).

# Code Samples
Dans un premier temps TreeFtp lance une connexion selon la présence ou non d'identifiants.
```java
public void launch() throws UnknownHostException, IOException {
		if (user != null && password != null) {
			canal.connect(user, password);
		} else {
			canal.connect();
		}
	}
```
Le canal se connecte à l'hote
```java
public Canal(String address, int port) {
		try {
			socket = new Socket(address, port);
			out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			in = socket.getInputStream();
			isr = new InputStreamReader(in);
			reader = new BufferedReader(isr);

		} catch (UnknownHostException e) {
			throw new RuntimeException("L'hôte specifié n'est pas reconnu");
		} catch (IOException e) {
			throw new RuntimeException("La connexion a été perdue ou interrompue");
		}
	}
```
L'authentification démarre
```java
public void connect(String user, String password) throws IOException {
		this.read();
		this.write("AUTH TLS");
		String log = this.read();
		if (log.startsWith("234")) {
			throw new RuntimeException("L'hôte specifié n'est pas accessible");
		}
		this.write("USER " + user);
		this.read();
		this.write("PASS " + password);
		log = this.read();
		if (!log.startsWith("230")) {// 230 signifie que l'authentification a réussi, si on ne l'a pas c'est que les identifiants sont incorrects
			throw new RuntimeException("Les identifiants sont incorrects, veuillez réessayer");
		}
		this.write("OPTS UTF8 ON");
		this.read();
	}
```

On affiche l'arborescence récursivement en passant en mode passif avec PASV pour créer un canal de récupération des données reçues grace à la commande LIST. On incrémente l'indentation pour l'affichage et ne pas dépasser la profondeur maximale.
```java
public void tree(int depth, int maxdepth, boolean print) throws IOException {
		if (depth < maxdepth || maxdepth == -1) {
			canal.write("PASV");
			String msg = canal.read();
			Canal c = passiveMode(msg); // Création du canal de récupération
			canal.write("LIST");
			canal.read();
			canal.read();
			String msg2 = c.read();
			while (msg2 != null) {
				print(depth, msg2, print);
				if (isExecutableDirectory(msg2)) { //vérification qu'il s'agit d'un dossier éxécutable pour lancer la récursivité
					try {
						canal.write("CWD " + getName(msg2));
						canal.read();
						tree(depth + 1, maxdepth, print);
						canal.write("CDUP");
						canal.read();
					} catch (Exception e) {
						// un dossier (ftp.free.fr pub/assistance par exemple) peut entraîner la perte de connexion et l'arrêt de l'affichage de l'arbre donc on l'ignore
					}
```
L'affichage diffère pour les liens et si le paramètre -p est activé les détails sont ajoutés.
```java
private void print(int depth, String line, boolean details) {
		String[] tab = line.split("\\s+");
		if (line.startsWith("l")) {
			if (details) {
				System.out.println("\t".repeat(depth) + "├" + String.join(" ", tab));// on affiche tout
			} else {
				System.out.println("\t".repeat(depth) + "├" + tab[8] + " -> " + tab[10]);// on affiche que le nom et la destination du lien
			}
		} else {
			if (details) {
				System.out.println("\t".repeat(depth) + "├" + String.join(" ", tab)); //on affiche tout
			} else {
				System.out.println("\t".repeat(depth) + "├" + tab[8]);// on affiche que le nom
			}
		}
	}
```
# Utilisation

Se placer à la racine.

Pour générer la javadoc il faut taper la commande suivante
```
mvn javadoc:javadoc
```
Pour générer le jar éxécutable :
```
mvn package
```

Pour lancer la commande TreeFtp :
```
java -jar target/TreeFtp-1.0-SNAPSHOT.jar host [user password] [-L=x] [-p]
```
exemple :  java -jar target/TreeFTP-1.0-SNAPSHOT.jar ftp.ubuntu.com -L=2 -p

[] : optionnel

-L=x : limiter à une profondeur maximale voulue avec x = la profondeur maximale

-p : l'affichage d'informations supplémentaires sur les fichiers (-p de la commande tree semblable à ls -l)