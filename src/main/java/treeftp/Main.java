package treeftp;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * @author Arthur Assima Prints the tree of files of an ftp server.
 */
public class Main {
	public static void main(String[] args) throws UnknownHostException, IOException {
		if (args.length > 5) {
			throw new RuntimeException("Nombre d'arguments invalide : serveur [user password] [-L=x] [-p]");
		}
		int depth = -1;
		boolean print = false;
		String user = null;
		String pass = null;
		for (int i = 1; i < args.length; i++) { // Vérification des arguments
			if (args[i].startsWith("-L=") && args[i].length() > 3) { // paramètre optionnel -L de tree (profondeur
																		// maximale)
				depth = Integer.parseInt(args[i].substring(args[i].indexOf("=") + 1));
			}
			if (args[i].equals("-p")) { // paramètre optionnel -p de tree (détails du fichier)
				print = true;
			} else if (user == null && !args[i].equals("-p") && !args[i].startsWith("-L=")) {
				user = args[i];
			} else if (pass == null && user != null) {
				pass = args[i];
			}
		}

		if (user != null && pass != null) {
			TreeFtp t = new TreeFtp(args[0], 21, user, pass);
			t.launch();// connection with the ftp server
			t.tree(0, depth, print);// tree's printing
		} else {
			TreeFtp t = new TreeFtp(args[0], 21);
			t.launch();// connection with the ftp server
			t.tree(0, depth, print);// tree's printing
		}

	}

}
