package treeftp;

import java.io.*;
import java.net.*;

/**
 * @author Arthur Assima
 */
public class TreeFtp {
	private String address;
	private int port;
	private String user;
	private String password;
	private Canal canal;

	/**
	 * Creates a file's tree from an ftp server
	 * 
	 * @param a the address of the server
	 * @param p the port to connect
	 */
	public TreeFtp(String a, int p) {
		address = a;
		port = p;
		user = "anonymous";
		password = "*********************";
		canal = new Canal(a, p);
	}

	/**
	 * Creates a file's tree from an ftp server
	 * 
	 * @param a    the address of the server
	 * @param p    the port to connect
	 * @param usr  the user to connect
	 * @param pass the password to connect
	 */
	public TreeFtp(String a, int p, String usr, String pass) {
		address = a;
		port = p;
		user = usr;
		password = pass;
		canal = new Canal(a, p);
	}

	/**
	 * Launch a connection to the ftp server
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void launch() throws UnknownHostException, IOException {
		if (user != null && password != null) {
			canal.connect(user, password);
		} else {
			canal.connect();
		}
	}

	/**
	 * Prints the ftp server's tree of files
	 * 
	 * @param depth the depth of the tree
	 * @throws IOException
	 */
	public void tree(int depth, int maxdepth, boolean print) throws IOException {
		if (depth < maxdepth || maxdepth == -1) {
			canal.write("PASV");
			String msg = canal.read();
			Canal c = passiveMode(msg);
			canal.write("LIST");
			canal.read();
			canal.read();
			String msg2 = c.read();
			while (msg2 != null) {
				print(depth, msg2, print);
				if (isExecutableDirectory(msg2)) {
					try {
						canal.write("CWD " + getName(msg2));
						canal.read();
						tree(depth + 1, maxdepth, print);
						canal.write("CDUP");
						canal.read();
					} catch (Exception e) {
						// un dossier (ftp.free.fr pub/assistance par exemple) peut entraîner la perte
						// de connexion et l'arret de l'affichage de l'arbre donc on l'ignore
					}
				}
				msg2 = c.read();
			}
		}
	}

	/**
	 * Prints a line from the tree
	 * 
	 * @param depth the depth of the line to print
	 * @param line  the line corresponding to the file
	 */
	private void print(int depth, String line, boolean details) {
		String[] tab = line.split("\\s+");
		if (line.startsWith("l")) {
			if (details) {
				System.out.println("\t".repeat(depth) + "├" + String.join(" ", tab));
			} else {
				System.out.println("\t".repeat(depth) + "├" + tab[8] + " -> " + tab[10]);
			}
		} else {
			if (details) {
				System.out.println("\t".repeat(depth) + "├" + String.join(" ", tab));
			} else {
				System.out.println("\t".repeat(depth) + "├" + tab[8]);
			}
		}
	}

	/**
	 * Gets the file's name
	 * 
	 * @param line the line with the file's informations
	 * @return the file's name
	 */
	private String getName(String line) {
		String[] tab = line.split("\\s+");
		String res = tab[8];
		return res;
	}

	/**
	 * Verifies if the file is an executable directory
	 * 
	 * @param line the line with all of the files' informations
	 * @return true if it's an executable file, false else
	 */
	public boolean isExecutableDirectory(String line) {
		String[] tab = line.split("\\s+");
		return tab[0].endsWith("x") && tab[0].startsWith("d");
	}

	/**
	 * Creates a new Canal to retrieve data in passive mode
	 * 
	 * @param msg the msg from the server after the PASV command
	 * @return a new canal connected to the ip address and port number given by the
	 *         server
	 */
	private Canal passiveMode(String msg) {
		String address = getAddress(msg);
		int port = getPort(msg);
		return new Canal(address, port);
	}

	/**
	 * Gets the address to connect with the ftp's passive mode
	 * 
	 * @param msg the response from the server with the address and the port number
	 * @return the ip address for the second Canal to retrieve data
	 */
	public String getAddress(String msg) {
		msg = msg.substring(msg.indexOf("(") + 1, msg.indexOf(")"));
		String[] s = msg.split(",", 4);
		String res = String.join(".", s);
		return res.substring(0, res.indexOf(","));
	}

	/**
	 * Gets the port number to connect with the ftp's passive mode
	 * 
	 * @param msg the response from the server with the address and the port number
	 * @return the port number for the second Canal to retrieve data
	 */
	public int getPort(String msg) {
		msg = msg.substring(msg.indexOf("(") + 1, msg.indexOf(")"));
		String[] s = msg.split(",");
		return Integer.parseInt(s[4]) * 256 + Integer.parseInt(s[5]);
	}

}