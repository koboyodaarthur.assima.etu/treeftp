package treeftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author Arthur Assima Canal class to communicate with the server
 */
public class Canal {
	private Socket socket;
	private OutputStream out;
	private PrintWriter printer;
	private InputStream in;
	private InputStreamReader isr;
	private BufferedReader reader;

	/**
	 * Canal between the client and the server
	 * 
	 * @param address IP address to connect
	 * @param port    the port number to connect
	 */
	public Canal(String address, int port) {
		try {
			socket = new Socket(address, port);
			out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			in = socket.getInputStream();
			isr = new InputStreamReader(in);
			reader = new BufferedReader(isr);

		} catch (UnknownHostException e) {
			throw new RuntimeException("L'hôte specifié n'est pas reconnu");
		} catch (IOException e) {
			throw new RuntimeException("La connexion a été perdue ou interrompue");
		}
	}

	/**
	 * Returns the server's response
	 * 
	 * @return the message from the server
	 * @throws IOException
	 */
	public String read() throws IOException {
		String msg = reader.readLine();
		// System.out.println(msg);
		return msg;
	}

	/**
	 * Writes a message to the server
	 * 
	 * @param s message to the server
	 * @throws IOException
	 */
	public void write(String s) throws IOException {
		printer.println(s);
	}

	/**
	 * Connects to the FTP server anonymously
	 * 
	 * @throws IOException
	 */
	public void connect() throws IOException {
		this.read();
		this.write("AUTH TLS");
		String log = this.read();
		if (log.startsWith("234")) {
			throw new RuntimeException("L'hôte specifié n'a pas été pas reconnu");
		}
		this.write("USER anonymous");
		this.read();
		this.write("PASS *********************");
		log = this.read();
		if (!log.startsWith("230")) {
			throw new RuntimeException("Les identifiants sont incorrects, veuillez réessayer");
		}
		this.write("OPTS UTF8 ON");
		this.read();
	}

	/**
	 * Connects to the FTP server with the user and password specified
	 * 
	 * @param user     user to connect
	 * @param password password to connect
	 * @throws IOException
	 */
	public void connect(String user, String password) throws IOException {
		this.read();
		this.write("AUTH TLS");
		String log = this.read();
		if (log.startsWith("234")) {
			throw new RuntimeException("L'hôte specifié n'a pas été pas reconnu");
		}
		this.write("USER " + user);
		this.read();
		this.write("PASS " + password);
		log = this.read();
		if (!log.startsWith("230")) {
			throw new RuntimeException("Les identifiants sont incorrects, veuillez réessayer");
		}
		this.write("OPTS UTF8 ON");
		this.read();
	}

}
