package treeftp;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Arthur Assima
 *
 *         Test some TreeFTP methods
 */
public class TestTreeFTP {

	/**
	 * Tests if the ip address and the port number given by the server is correctly
	 * transformed
	 */
	@Test
	public void testIPtransformation() {
		TreeFtp t = new TreeFtp("ftp.free.fr", 21);
		String s = "227 Entering Passive Mode (212,27,60,27,249,199).";
		assertEquals("212.27.60.27", t.getAddress(s));
		assertEquals(63943, t.getPort(s));
	}

	/**
	 * Tests if the file is an executable directory
	 */
	@Test
	public void testisExecutableDirectory() {
		TreeFtp t = new TreeFtp("ftp.free.fr", 21);
		String vrai = "drwxr-xr-x 6 997 997 4096 Feb 27 2010 pool";
		String faux = "lrwxr-xr-x 6 997 997 4096 Feb 27 2010 pool";
		String faux2 = "drwxr-xr-- 6 997 997 4096 Feb 27 2010 pool";
		assertTrue(t.isExecutableDirectory(vrai));
		assertFalse(t.isExecutableDirectory(faux));
		assertFalse(t.isExecutableDirectory(faux2));
	}
}
